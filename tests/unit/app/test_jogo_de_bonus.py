import unittest
from app.jogo_de_bonus_I import Jogo_de_bonus


class Test_jogo_de_bonus(unittest.TestCase):
    def test_calc(self):
        jogo = Jogo_de_bonus()

        self.assertEqual(jogo.calc([1, 2, 5]), 50)
        self.assertEqual(jogo.calc([5, 4, 3]), 62)
        self.assertEqual(jogo.calc([7, 7, 9, 7]), 242)
        self.assertEqual(jogo.calc([2, 8, 6, 10, 7, 6, 10, 1, 5, 8]), 17372)
