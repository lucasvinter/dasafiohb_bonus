import unittest
from app.Square_every_digit import square_digits


class Test_penalty_for_speeding(unittest.TestCase):
    def test_metodo_penalty(self):
        self.assertEqual(square_digits(9119), 811181)
        self.assertEqual(square_digits(46), 1636)
        self.assertEqual(square_digits(210), 410)
