import unittest
from unittest.mock import patch, Mock

from app.jogo2 import Jogo


class Test_penalty_for_speeding(unittest.TestCase):
    @patch('app.jogo2.permutations')
    def test_metodo_tranformar_permutacoes_em_lista(self, mock_permutations):
        jogo = Jogo(['1', '100'])
        mock_permutations.permutations = Mock()
        jogo.get_permutacoes = Mock(return_value=(('1', '100'), ('100', '1')))

        self.assertEqual(jogo.pegar_minimo_multa_possivel(), '1001')




