import unittest
from app.menor_palavra import Menor_palavra

class Test_menor_palavra(unittest.TestCase):
    def test_metodo_find_sort_pega_tamanho_da_menor_palavra(self):
        palavra = Menor_palavra()
        self.assertEqual(palavra.find_short("bitcoin take over the world maybe who knows perhaps"), 3)
        self.assertEqual(palavra.find_short("The meeting Squads Review - Sprint 02 is cancelled"), 1)
        self.assertEqual(palavra.find_short("HBSIS SOLUÇÕES TEC. DA INFORMAÇÃO LTDA."), 2)


