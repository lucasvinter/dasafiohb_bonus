import unittest
from app.penalty_for_speeding import Penalty_for_speeding


class Test_penalty_for_speeding(unittest.TestCase):
    def test_metodo_penalty(self):
        penalty_for_speeding = Penalty_for_speeding()

        self.assertEqual(penalty_for_speeding.penalty(['45', '30', '50', '1']), '1304550')
        self.assertEqual(penalty_for_speeding.penalty(['100', '10', '1']), '100101')
        self.assertEqual(penalty_for_speeding.penalty(['32', '3']), '323')
        self.assertEqual(penalty_for_speeding.penalty(['70', '46', '4', '19']), '1944670')
        self.assertEqual(penalty_for_speeding.penalty(['71', '82', '42', '34', '90']), '3442718290')
        self.assertEqual(penalty_for_speeding.penalty(['31', '97', '6', '78']), '3167897')

