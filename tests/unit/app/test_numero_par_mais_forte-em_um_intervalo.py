import unittest
from app.numero_par_mais_forte_em_um_intervalo import Numeros


class Test_numeros(unittest.TestCase):
    def test_strongest_even_testar_o_numero_mais_divisivel_par(self):
        numero = Numeros()
        self.assertEqual(numero.strongest_even(20, 16), 16)
        self.assertEqual(numero.strongest_even(12, 100), 64)
        self.assertEqual(numero.strongest_even(30, 15), 15)

