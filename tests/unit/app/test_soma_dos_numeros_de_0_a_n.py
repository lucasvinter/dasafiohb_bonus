import unittest
from app.soma_dos_numeros_de_0_a_n import ShowSequence

class TestShowSequence(unittest.TestCase):
    def test_soma_dos_numeros_de_0_a_n(self):
        show_sequence = ShowSequence()
        self.assertEqual(show_sequence.show_sequence(15), "0+1+2+3+4+5+6+7+8+9+10+11+12+13+14+15 = 120")

    def test_show_sequence_return_0_se_n_igual_0(self):
        show_sequence = ShowSequence()
        self.assertEqual(show_sequence.show_sequence(0), '0=0')

    def test_show_sequence_return_n_menor_que_0(self):
        show_sequence = ShowSequence()
        self.assertEqual(show_sequence.show_sequence(-1), '-1<0')
