class Jogo_de_bonus:

    def calc(self, cartas: list) -> int:
        matriz = [0] * (len(cartas) + 1)
        for carta in range(len(cartas)):
            matriz = [2 * max(cartas[i] + matriz[i + 1], cartas[i + carta] + matriz[i]) for i in range(len(cartas) - carta)]
        return matriz[0]
