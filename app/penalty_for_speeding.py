class Penalty_for_speeding:

    def penalty(self, lista_de_multas):
        lista_de_multas, tamanho_maximo = list(map(str, lista_de_multas)), max(map(len, lista_de_multas))
        return ''.join(sorted(lista_de_multas, key=lambda s: s.ljust(tamanho_maximo, s[-1])))





