from itertools import permutations
class Jogo:
    def __init__(self, lista_de_multas: list):
        self._lista_de_multas = lista_de_multas

    def get_permutacoes(self) -> tuple:
        permutacoes = permutations(self._lista_de_multas)
        return tuple(permutacoes)

    def pegar_minimo_multa_possivel(self):
        lista_de_multas_possiveis = []

        for permutacao in self.get_permutacoes():
            transformar_lista_de_permutacoes_em_str = ''.join(permutacao)
            lista_de_multas_possiveis.append(transformar_lista_de_permutacoes_em_str)

        return min(lista_de_multas_possiveis)


test = Jogo(['10', '100', '1'])
print(test.pegar_minimo_multa_possivel())
