class ShowSequence:
    def show_sequence(self, n):
        soma = 0
        lista = []
        if n > 0:
            for c in range(n + 1):
                soma += c
                lista.append(str(c))
        elif n == 0:
            return '0=0'
        else:
            return f"{n}<0"

        return f"{'+'.join(lista)} = {soma}"

